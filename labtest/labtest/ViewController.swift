//
//  ViewController.swift
//  labtest
//
//  Created by user227784 on 11/17/22.
//

import UIKit
struct user: Codable {
    let character: String
    let answer: String
    
}

struct sampleRecord: Codable {
    let maps: [user]
}

class ViewController: UIViewController {
    private var jsoncurri : String!;
    private var indexi=0;
    private var quest = [String]();
    private var answerapa = [String]();
    private var anslistdelvery=[String]();
    private var mixnum=[Int]();
    private var sampleRecordObjall : String!
    func readLocalJSONFile(forName name: String) -> Data? {
        do {
            if let filePath = Bundle.main.path(forResource: name, ofType: "json") {
                let fileUrl = URL(fileURLWithPath: filePath)
                let data = try Data(contentsOf: fileUrl)
                return data
            }
        } catch {
            print("error: \(error)")
        }
        return nil
    }
    
    func parse(jsonData: Data) -> sampleRecord? {
        do {
            let decodedData = try JSONDecoder().decode(sampleRecord.self, from: jsonData)
            return decodedData
        } catch {
            print("error: \(error)")
        }
        return nil
    }
    
    func mixing(){
        let randomNumber1 = Int(arc4random_uniform(450))
        let randomNumber2 = Int(arc4random_uniform(450))
        while randomNumber1 == randomNumber2{
            let randomNumber2 = Int(arc4random_uniform(450))
            
        }
        let randomNumber3 = Int(arc4random_uniform(450))
        while randomNumber1 == randomNumber3 || randomNumber2 == randomNumber3{
            let randomNumber3 = Int(arc4random_uniform(450))
            
        }
        
        var nums = [0,1,2,3];
        
        while nums.count > 0 {

            // random key from array
            let arrayKey = Int(arc4random_uniform(UInt32(nums.count)))

            // your random number
            let randNum = nums[arrayKey]

            // make sure the number isnt repeated
            nums.swapAt(arrayKey, nums.count-1)
            mixnum.append(nums.removeLast())
        }
        
        anslistdelvery.append(answerapa[randomNumber1]);
        anslistdelvery.append(answerapa[randomNumber2]);
        anslistdelvery.append(answerapa[randomNumber3]);
        anslistdelvery.append(answerapa[indexi]);
        //print("XAXAXXAXAXAX",anslistdelvery[mixnum[0]]);

        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        // Do any additional setup after loading the view.
        let jsonData = readLocalJSONFile(forName: "russianverbs500")
        if let data = jsonData {
            if let sampleRecordObj = parse(jsonData: data) {
                //You can read sampleRecordObj just like below.
                print(sampleRecordObj.maps[0].character);
                for ob in sampleRecordObj.maps{
                    quest.append(ob.character);
                    answerapa.append(ob.answer)
                }
                
            }
            
        }
    }
        

        
    


    @IBOutlet weak var LabelQuestion: UILabel!
    
    @IBOutlet weak var LabelResult: UILabel!
    /*
    @IBOutlet weak var answer1: UIButton!
    @IBOutlet weak var answer2: UIButton!
    @IBOutlet weak var answer3: UIButton!
    @IBOutlet weak var answer4: UIButton!
     */
    
    @IBOutlet weak var anss1: UIButton!
    
    @IBOutlet weak var anss4: UIButton!
    @IBOutlet weak var anss3: UIButton!
    @IBOutlet weak var anss2: UIButton!
    @IBAction func nextbut(_ sender: Any) {
        indexi+=1;
        LabelQuestion.text=quest[indexi];
        mixing();
        anss1.setTitle(anslistdelvery[mixnum[0]],for: .normal);
        anss2.setTitle(anslistdelvery[mixnum[1]],for: .normal);
        anss3.setTitle(anslistdelvery[mixnum[2]],for: .normal);
        anss4.setTitle(anslistdelvery[mixnum[3]],for: .normal);
        //var answersfordelevery=[String] ();
        
        
    }
    
    
    @IBAction func anss1(_ sender: Any) {
        //anss1.isEnabled=true;
        LabelResult.text="Wrong"
    }
    
    @IBAction func anss2(_ sender: Any) {
        LabelResult.text="Correct"
        
    }
    
    @IBAction func anss4(_ sender: Any) {
        LabelResult.text="Wrong"
        
    }
    
    @IBAction func anss3(_ sender: Any) {
        LabelResult.text="Wrong"
        
    }
    
}

